---
title: Tracking for Safety
description: Using RTLS to track workers for the insurance of their safety 
author: Robin Berweiler
keywords: safety, tracking, workers
url: https://blueberrywaltz.gitlab.io/safety-tracking-concept
marp: true
image: ![50%](assets/safety_avatar.jpg)
---

# Prevention of Accidents in with intelligent tracking software



---
# Costs of Accidents

* Estimated cost of fatal and nonfatal injuries worldwide to $13 billion annually
* Deaths are estimated to be 40% of the total cost
* Nonfatal injuries and illnesses represent 60% of the total cost


---

# Estimated costs of work-related injuries (All types of employment)

![](./assets/2.png)

---

# Medical costs for work-related injuries in construction  (All types of employment)

![](./assets/3.png)

---

# Estimated costs of work-related injuries     (Wage-and-salary employment)

![](assets/1.png)



---

# Work Injury Costs - 2020 (NSC - US)

| Cost      |  |
| ----------- | ----------- |
| Total in 2020      | $163.9 billion       |
| Per worker   | $1,100        |
| Per death   | 1,310,000        |
| Per medically consulted injury   | $44,000        |

---

# Time Lost - 2020 (NSC - US)


| Days lost      |  |
| ----------- | ----------- |
| Total in 2020      | 99,000,000       |
| Due to injuries in 2020   | 65,000,000  |
| Per death   | 1,310,000        |
| In future years from 2020 injuries   | 50,000,000        |

---
# Stories

* There are thousands of stories available of millions of accidents worldwide
* The fatal four (most common accidents) are:
  * Falling
  * Electrocution
  * **Getting caught (entangled)**
  * Getting struck 
* Temp worker are more likely to get involved in accidents due to lack of education


---

# Examples of Entanglement

* Worker dies when he was **pulled into a hummus grinder** he was cleaning and crushed between two large rotating screws
* Worker dies after **getting pulled in by lathe machine** at work inside metal factory in Russia
* Worker dies when he was **run over by a trash compactor** while directing traffic at the county landfill
* Worker **got pulled into mortar mixer** while trying to shut it down
---
# Other Non-fatal Risks of Entanglement

* scalping
* amputation 
* de-gloving

---
# Hazard of entanglement
@startuml

@startmindmap
+ Hazard of entanglement
++ into Machine
+++ loose items
++++ clothing
++++ hair
++++ gloves
++++ cleaning brush
++++ rags or other material
+++_ Body Parts
++++ rotating surfaces
+++++ single
+++++ counterclockwise
+++++ and tangentially moving parts
++++ materials in motion
++++ caught in projections or gaps 
+++_ Worker stupidity
++ sand/gravel
++ construction debris

'tag::left[]
-- ...
'end::left[]


@endmindmap

@enduml

---
# Concept

What could be a solution?
* Smart monitoring of machines
  * Reveal hazards and educate workers
  * Detect hazards and prevent them
  * Notice accidents fast enough to prevent the worst
  
---
# Concept

![](./assets/ConceptModel.PNG)

---

# Caffe

Caffe is an open-source deep learning framework developed from machine learning which serves as image classification and image segmentation models. It was written in C++ but can be used as well with Python. 

Importance of Caffe:

* Extensible Code --> easier for developers to deal with
* Expressive Architecture --> easier to use without hard-coding efforts
* Speed --> can process over 60 million images per day
* Support --> can always get developer support from user groups or the GitHub platform

This can be used to feed the cameras various possibilities of how injuries can occur which would lead to the machine learning from itself and preventing the accident from occurring.  
